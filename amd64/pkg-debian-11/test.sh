#!/bin/sh

set -e

echo "Clone lava.git"
git clone https://gitlab.com/arturaspaulius/lava.git /root/lava
cd /root/lava
echo "done"
echo

echo "Check bullseye dependencies"
PACKAGES=$(./share/requires.py -p lava-dispatcher -d debian -s bullseye -n)
for pkg in $PACKAGES
do
  echo "* $pkg"
  dpkg -l "$pkg" > /dev/null
done

PACKAGES=$(./share/requires.py -p lava-server -d debian -s bullseye -n)
for pkg in $PACKAGES
do
  echo "* $pkg"
  dpkg -l "$pkg" > /dev/null
done

echo "Build the debian package"
./share/debian-dev-build.sh -o build -s bullseye
echo "done"
