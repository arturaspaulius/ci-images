#!/bin/sh

set -e

LC_ALL=C.UTF-8 LANG=C.UTF-8 black --help
pycodestyle --help
radon --help
